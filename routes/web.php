<?php

use App\Http\Controllers\MemberController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

/* utama*/

Route::get('/', function () {
    return view('main');
});

Route::get('/member/login', [MemberController :: class, 'login'] );

Route::get('/member/register', [MemberController :: class, 'register'] );

Route::get('/member/member', [MemberController :: class, 'member'] );

Route::get('/member/profile', [MemberController :: class, 'profile'] );


Route :: get('/kelas', function(){
    return view('kelas');
});
