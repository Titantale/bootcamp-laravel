<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use function Ramsey\Uuid\v1;

class MemberController extends Controller
{
    public function index(){
        return view('member.dashboard');
    }

    public function profile(){
        
    }

    public function login(){
        return view('member.login');
    }

    public function register(){
        return view('member.register');
    }
}
